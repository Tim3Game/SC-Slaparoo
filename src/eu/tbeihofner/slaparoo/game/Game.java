package eu.tbeihofner.slaparoo.game;

import eu.tbeihofner.slaparoo.Main;
import eu.tbeihofner.slaparoo.utils.Yamls;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.UUID;

public class Game {

    static Integer countdown;   //Runnable
    static Integer count = 10;  //Counting
    public static GameState state;  //Game state

    public static Location lobbyLoc;    //Lobby spawn location
    public static Location gameLoc;     //Game spawn location

    public static Integer remaining = 0;    //Remaining Players

    public static enum GameState{
        WAITING, STARTING, INGAME
    }

    public static void Countdown(){
        state = GameState.STARTING;
        countdown = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin,  new Runnable() {
            @Override
            public void run() {
                if(count > 0){
                    if (count == 10 || count == 5) {
                        Bukkit.broadcastMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GREEN + "Hra začína za " + count + " sekúnd");
                    }
                    if (count <= 3 && count > 1) {
                        Bukkit.broadcastMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GREEN + "Hra začína za " + count + " sekundy");
                    }
                    if (count == 1) {
                        Bukkit.broadcastMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GREEN + "Hra začína za " + count + " sekundu");
                    }
                }else{
                    String plays = " ";
                    Bukkit.broadcastMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GOLD + "Hra sa začala !");
                    for (Player players : Bukkit.getOnlinePlayers()) {
                        Players.players.add(players.getUniqueId());
                        plays = plays + " " + players.getName();
                        players.getInventory().clear();
                        players.getInventory().setItem(0, cookie());
                        players.teleport(Game.gameLoc);
                        remaining = remaining + 1;
                    }

                    state = GameState.INGAME;
                    Bukkit.getScheduler().cancelTask(countdown);
                    count = 10;
                }
                count--;
            }
        }, 0, 20);
    }

    public static void EndGame(){
        String name = " ";
        for (UUID uuid : Players.players) {
            if(Players.players.size() > 2) {
                name = name + Bukkit.getPlayer(uuid).getName();
            } else {
                name = Bukkit.getPlayer(uuid).getName();
            }
        }
        if (name == " ") {
            Bukkit.broadcastMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GREEN + "Hra skončila ! Nevyhral nikto, škoda :/");
        } else {
            Bukkit.broadcastMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GREEN + "Hra skončila ! Vyhral hráč " + name + ", gratuľujeme !");
        }
        Game.state = Game.GameState.WAITING;

        countdown = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin,  new Runnable() {
            @Override
            public void run() {
                if (count == 10 || count == 5) {
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + Main.prefix + ChatColor.RED + "Server sa reštartuje za " + count + " sekúnd!");
                }
                if (count <= 3 && count > 1) {
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + Main.prefix + ChatColor.RED + "Server sa reštartuje za " + count + " sekundy!");
                }

                if (count == 1) {
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + Main.prefix + ChatColor.RED + "Server sa reštartuje za " + count + " sekundu!");
                }
                if (count == 0) {
                    Bukkit.getServer().spigot().restart();
                }
                count--;
            }
        }, 0, 20);
    }

    public static void stopTime(){
        Bukkit.getServer().getScheduler().cancelAllTasks();
        count = 10;
    }


    public static ItemStack cookie(){
        ItemStack itemStack = new ItemStack(Material.COOKIE);
        ItemMeta itemMeta = itemStack.getItemMeta();
        ArrayList<String> lore = new ArrayList<>();

        itemMeta.setDisplayName(ChatColor.AQUA + "Sušienka od starej mamy");
        lore.add(ChatColor.GREEN + "Pamätaj, túto sušienku si dostaľ za svoju statočnosť !");
        itemMeta.addEnchant(Enchantment.KNOCKBACK, 3, true);

        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }
}
