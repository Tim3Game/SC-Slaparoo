package eu.tbeihofner.slaparoo.utils;

import eu.tbeihofner.slaparoo.Main;
import eu.tbeihofner.slaparoo.game.Game;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;


public class commands implements CommandExecutor {

    public static ArrayList<String> users = new ArrayList<String>();
    public static ArrayList<UUID> votes = new ArrayList<UUID>();

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            Player p = ((Player) sender).getPlayer();

            if (commandLabel.equalsIgnoreCase("slap")) {
                if (args.length == 1) {
                    if (args[0].equals("start")) {
                        Game.Countdown();
                    }
                    if (args[0].equals("stop")) {
                        Game.stopTime();
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.BLUE + "Hra bola zastavená!");
                    }
                } else if (args.length >= 2) {
                    if (args[0].equals("lobby")) {
                        if (args[1].equals("set")) {
                            if (args[2].equals("loc")) {
                                Yamls.config.set("lobby.loc.world", p.getWorld().getName());
                                Yamls.config.set("lobby.loc.x", p.getLocation().getX());
                                Yamls.config.set("lobby.loc.y", p.getLocation().getY());
                                Yamls.config.set("lobby.loc.z", p.getLocation().getZ());
                                Yamls.config.set("lobby.loc.yaw", p.getLocation().getYaw());
                                Yamls.config.set("lobby.loc.pitch", p.getLocation().getPitch());
                                Yamls.saveYML(Yamls.yaml.CONFIG);
                            }
                            p.sendMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GREEN + "Lobby bolo nastavené!");
                        } else {
                            p.sendMessage(ChatColor.DARK_RED + Main.prefix + "Zlý príkaz: " + ChatColor.RED + "/slap lobby set loc");
                        }
                    }
                    if (args[0].equals("game")) {
                        if (args[1].equals("set")) {
                            if (args[2].equals("loc")) {
                                Yamls.config.set("game.loc.world", p.getWorld().getName());
                                Yamls.config.set("game.loc.x", p.getLocation().getX());
                                Yamls.config.set("game.loc.y", p.getLocation().getY());
                                Yamls.config.set("game.loc.z", p.getLocation().getZ());
                                Yamls.config.set("game.loc.yaw", p.getLocation().getYaw());
                                Yamls.config.set("game.loc.pitch", p.getLocation().getPitch());
                                Yamls.saveYML(Yamls.yaml.CONFIG);
                            }
                            if (args[2].equals("minPlayers")) {
                                if (args[3] != null || Integer.valueOf(args[3]) != 0) {
                                    Yamls.config.set("game.loc.minPlayers", Integer.valueOf(args[3]));
                                    Yamls.saveYML(Yamls.yaml.CONFIG);
                                } else {
                                    p.sendMessage(ChatColor.DARK_RED + Main.prefix + "Zlé číslo: " + ChatColor.RED + "Zadali ste zle číslo, alebo ste ho nezadali vôbec!");
                                }
                            }
                            if (args[2].equals("lowest")) {
                                Yamls.config.set("game.loc.lowest", p.getLocation().getY());
                                Yamls.saveYML(Yamls.yaml.CONFIG);
                            }
                            p.sendMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GREEN + "Mapa bola nastavená!");
                        } else {
                            p.sendMessage(ChatColor.DARK_RED + Main.prefix + "Zlý príkaz: " + ChatColor.RED + "/slap game set loc|lowest|minPlayers");
                        }
                    }

                } else {
                    p.sendMessage(ChatColor.BOLD + Main.prefix + ChatColor.DARK_RED + ChatColor.BOLD + "SC SLAPAROO - " + Main.version);
                    p.sendMessage(ChatColor.BOLD + Main.prefix + ChatColor.RED + "Zlý príkaz! Skúste:");
                    p.sendMessage(ChatColor.BOLD + Main.prefix + ChatColor.RED + "/slap stop");
                    p.sendMessage(ChatColor.BOLD + Main.prefix + ChatColor.RED + "/slap lobby set loc");
                    p.sendMessage(ChatColor.BOLD + Main.prefix + ChatColor.RED + "/slap game set loc|lowest|minPlayers");
                    p.sendMessage(ChatColor.BOLD + Main.prefix + ChatColor.DARK_RED + ChatColor.BOLD + "This minigame was only created for SC recruitment");
                    p.sendMessage(ChatColor.BOLD + Main.prefix + ChatColor.DARK_RED + ChatColor.BOLD + "2017 © TBeihofner GmbH & Stylecraft");
                }
            }
        }
        return false;
    }
}
