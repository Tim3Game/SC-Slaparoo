package eu.tbeihofner.slaparoo;

import eu.tbeihofner.slaparoo.game.Game;
import eu.tbeihofner.slaparoo.listeners.EntityDamageByEntity;
import eu.tbeihofner.slaparoo.listeners.PlayerJoin;
import eu.tbeihofner.slaparoo.listeners.PlayerLeave;
import eu.tbeihofner.slaparoo.listeners.PlayerMove;
import eu.tbeihofner.slaparoo.utils.Yamls;
import eu.tbeihofner.slaparoo.utils.commands;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{

    public static Plugin plugin;
    public static String prefix = "[Slaparoo] ";
    public static String version = "1.3-ALPHA";

    @Override
    public void onEnable() {
        plugin = this;

        Yamls.createFiles(this);
        registerCommands();
        registerListeners();

        if (Yamls.config.get("lobby") == null) {
            Bukkit.broadcastMessage(prefix + ChatColor.DARK_RED + "Hra nie je nastavená, nemôže byť spustená!");
        } else {
            World worldLobby = Bukkit.getWorld(Yamls.config.getString("lobby.loc.world"));
            int xLobby = Yamls.config.getInt("lobby.loc.x");
            int yLobby = Yamls.config.getInt("lobby.loc.y");
            int zLobby = Yamls.config.getInt("lobby.loc.z");
            int yawLobby = Yamls.config.getInt("lobby.loc.yaw");
            int pitchLobby = Yamls.config.getInt("lobby.loc.pitch");

            if(yawLobby != 0 || pitchLobby != 0) {
                Game.lobbyLoc = new Location(worldLobby, xLobby, yLobby, zLobby, yawLobby, pitchLobby);
            } else {
                Game.lobbyLoc = new Location(worldLobby, xLobby, yLobby, zLobby);
            }


            World worldGame = Bukkit.getWorld(Yamls.config.getString("game.loc.world"));
            int xGame = Yamls.config.getInt("game.loc.x");
            int yGame = Yamls.config.getInt("game.loc.y");
            int zGame = Yamls.config.getInt("game.loc.z");
            int yawGame = Yamls.config.getInt("game.loc.yaw");
            int pitchGame = Yamls.config.getInt("game.loc.pitch");

            if(yawGame != 0 || pitchGame != 0) {
                Game.gameLoc = new Location(worldGame, xGame, yGame, zGame, yawGame, pitchGame);
            } else {
                Game.gameLoc = new Location(worldGame, xGame, yGame, zGame);
            }
        }
    }

    private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new EntityDamageByEntity(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new PlayerLeave(), this);
        pm.registerEvents(new PlayerMove(), this);
    }

    private void registerCommands() {
        getCommand("slap").setExecutor(new commands());
    }

    @Override
    public void onDisable() {

    }
}
