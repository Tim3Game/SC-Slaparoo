package eu.tbeihofner.slaparoo.listeners;

import eu.tbeihofner.slaparoo.Main;
import eu.tbeihofner.slaparoo.game.Game;
import eu.tbeihofner.slaparoo.game.Players;
import eu.tbeihofner.slaparoo.utils.Yamls;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerLeave implements Listener {

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent e) {
        Players.players.remove(e.getPlayer().getUniqueId());
        Game.remaining = Game.remaining - 1;
        if (Players.players.size() > Yamls.config.getInt("game.loc.minPlayers")) {
            e.setQuitMessage(ChatColor.DARK_RED + Main.prefix + ChatColor.RED + "Hráč " + e.getPlayer().getName() + " odišiel z hry");
        } else {
            e.setQuitMessage(ChatColor.DARK_RED + Main.prefix + ChatColor.RED + "Hráč " + e.getPlayer().getName() + " odišiel z hry");
            Bukkit.broadcastMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GREEN + "Potrebný hráči na začanie hry: " + (Yamls.config.getInt("game.loc.minPlayers") - (Bukkit.getOnlinePlayers().size() - 1)));
        }
        if (Players.players.size() < Yamls.config.getInt("game.loc.minPlayers")) {
            if (Game.state == Game.GameState.STARTING) {
                Game.stopTime();
            }
            if (Game.state == Game.GameState.INGAME) {
                Game.EndGame();
            }
        }
    }
}
