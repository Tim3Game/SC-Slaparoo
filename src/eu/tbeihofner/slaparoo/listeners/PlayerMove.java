package eu.tbeihofner.slaparoo.listeners;

import eu.tbeihofner.slaparoo.Main;
import eu.tbeihofner.slaparoo.game.Game;
import eu.tbeihofner.slaparoo.game.Players;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.UUID;

public class PlayerMove implements Listener{

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (Game.state == Game.GameState.INGAME && Players.players.contains(e.getPlayer().getUniqueId())) {
            if (e.getPlayer().getLocation().getY() <= Main.plugin.getConfig().getInt("game.loc.lowest")) {
                Players.players.remove(e.getPlayer().getUniqueId());
                e.getPlayer().setGameMode(GameMode.SPECTATOR);
                e.getPlayer().teleport(Game.gameLoc.add(0, 5, 0));
                Game.remaining = Game.remaining - 1;
                if(EntityDamageByEntity.lastDamage.containsKey(e.getPlayer().getUniqueId()) || EntityDamageByEntity.lastDamage.get(e.getPlayer().getUniqueId()) != null){
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + Main.prefix + ChatColor.RED + "Hráča " + e.getPlayer().getName() + " zhodil nezbedník " + Bukkit.getPlayer(EntityDamageByEntity.lastDamage.get(e.getPlayer().getUniqueId())).getName() + "!");
                } else {
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + Main.prefix + ChatColor.RED + "Hráč " + e.getPlayer().getName() + " spadol !");
                }

                if(Game.remaining == 1) {
                    Game.EndGame();
                }
            }
        }

    }
}
