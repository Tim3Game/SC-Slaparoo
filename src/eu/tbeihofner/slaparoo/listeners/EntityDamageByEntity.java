package eu.tbeihofner.slaparoo.listeners;

import eu.tbeihofner.slaparoo.game.Game;
import eu.tbeihofner.slaparoo.game.Players;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.HashMap;
import java.util.UUID;

public class EntityDamageByEntity implements Listener{


    public static HashMap<UUID, UUID> lastDamage = new HashMap<>();

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {
            if(Game.state == Game.GameState.WAITING || Game.state == Game.GameState.STARTING) {
                e.setCancelled(true);
            }
            if(Game.state == Game.GameState.INGAME) {
                if(lastDamage.containsKey(e.getEntity().getUniqueId())) {
                    lastDamage.replace(e.getEntity().getUniqueId(), e.getDamager().getUniqueId());
                } else {
                    lastDamage.put(e.getEntity().getUniqueId(), e.getDamager().getUniqueId());
                }
            }
        } else {
            e.setCancelled(true);
        }
    }
}
