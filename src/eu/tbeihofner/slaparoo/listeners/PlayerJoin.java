package eu.tbeihofner.slaparoo.listeners;

import eu.tbeihofner.slaparoo.Main;
import eu.tbeihofner.slaparoo.game.Game;
import eu.tbeihofner.slaparoo.game.Players;
import eu.tbeihofner.slaparoo.utils.Yamls;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        if (Game.state != Game.GameState.INGAME) {
            e.getPlayer().setGameMode(GameMode.SURVIVAL);
            e.getPlayer().setHealth(20);
            e.getPlayer().setFoodLevel(20);
            e.getPlayer().getInventory().clear();

            e.setJoinMessage(ChatColor.DARK_GREEN + Main.prefix + "Hráč " + e.getPlayer().getName() + " sa zapojiľ do hry");
            if (Yamls.config.get("lobby") != null) {
                e.getPlayer().teleport(Game.lobbyLoc);
            }

            if (Yamls.config.getInt("game.loc.lowest") != 0 || Yamls.config.getInt("game.loc.minPlayers") != 0 || Yamls.config.get("game") != null) {
                if (Yamls.config.get("lobby") != null) {
                    if (Yamls.config.getInt("game.loc.minPlayers") != 0) {
                        if (Bukkit.getOnlinePlayers().size() >= Yamls.config.getInt("game.loc.minPlayers")) {
                            Game.Countdown();
                        } else {
                            Bukkit.broadcastMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GREEN + "Potrebný hráči: " + (Yamls.config.getInt("game.loc.minPlayers") - Bukkit.getOnlinePlayers().size()));
                        }
                    } else {
                        if (Bukkit.getOnlinePlayers().size() >= 2) {
                            Game.Countdown();
                        } else {
                            Bukkit.broadcastMessage(ChatColor.DARK_GREEN + Main.prefix + ChatColor.GREEN + "Potrebný hráči: " + (2 - Bukkit.getOnlinePlayers().size()));
                        }
                    }
                } else {
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + Main.prefix + ChatColor.RED + "Lobby nie je nastavené, kontaktujte admina !");
                }
            } else {
                Bukkit.broadcastMessage(ChatColor.DARK_RED + Main.prefix + ChatColor.RED + "Mapa nie je nastavená, kontaktujte admina !");
            }
        } else {
            Players.players.remove(e.getPlayer().getUniqueId());
            e.getPlayer().setGameMode(GameMode.SPECTATOR);
            e.setJoinMessage(ChatColor.BLUE + Main.prefix + "Pozorovateľ " + e.getPlayer().getName() + " sa prišiel pozrieť na hru");
        }
    }
}
